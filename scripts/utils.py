import os


def get_file_in_dir(dir_path, file_type):
    all_files = os.listdir(dir_path)
    desired_type_files = [os.path.join(dir_path, file) for file in all_files if file.endswith(file_type)]
    return desired_type_files
