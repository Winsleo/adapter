#include "ros_utility.hpp"
#include <ros/package.h>
#include <rosbag/bag.h>
#include <sensor_msgs/Imu.h>
#include "livox_ros_driver/CustomMsg.h"

const std::string packageName = "adapter";

class PreprocessorMetacam
{
public:
    ros::Subscriber sub_imu;
    ros::Subscriber sub_lidar;
    std::vector<ImageProcessor> imageProcessors;
    std::string outputDir;
    int waitPeriod = -1;

    PreprocessorMetacam(const std::shared_ptr<ros::NodeHandle> &nh, bool compressed = false)
    {
        std::string packagePath = ros::package::getPath(packageName);
        nh->param<std::string>("output_dir", outputDir, packagePath + "/result/");
        CommonTools::IfNotExistThenCreate(outputDir);

        int num_camera;
        nh->param<int>("/max_cameras", num_camera, 2);
        cameraBagSavers.reserve(num_camera);
        std::vector<std::string> topicCameraIn;
        for(size_t i=0; i<num_camera; i++) {
            std::vector<double> distortionCoeffs;
            std::vector<double> intrinsics;
            std::vector<int> resolution;
            std::vector<int> outResolution;
            std::vector<double> vectorBuf;

            std::string topicCameraIn;
            std::string topicCameraOut;
            std::string camId = std::to_string(i);
            nh->param<std::string>("/cam" + camId + "/rostopic", topicCameraIn, "");
            nh->param<std::string>("/cam" + camId + "/rostopic_out_undistort", topicCameraOut, "");
            nh->param<std::vector<double>>("/cam" + camId + "/distortion_coeffs", distortionCoeffs, std::vector<double>());
            nh->param<std::vector<double>>("/cam" + camId + "/intrinsics", intrinsics, std::vector<double>());
            nh->param<std::vector<int>>("/cam" + camId + "/resolution", resolution, std::vector<int>());
            nh->param<std::vector<int>>("/cam" + camId + "/out_resolution", outResolution, std::vector<int>());
            nh->param<std::vector<double>>("/cam" + camId + "/T_lidar_cam", vectorBuf, std::vector<double>());

            ROS_INFO("cam%lu/distortionCoeffs: %s", i, distortionCoeffs.size() > 1 ? (std::to_string(distortionCoeffs[0]) + " " + std::to_string(distortionCoeffs[1]) + " " + std::to_string(distortionCoeffs[2]) + " " + std::to_string(distortionCoeffs[3])).c_str() : "None");
            ROS_INFO("cam%lu/intrinsics: %s", i, intrinsics.size() > 1 ? (std::to_string(intrinsics[0]) + " " + std::to_string(intrinsics[1]) + " " + std::to_string(intrinsics[2]) + " " + std::to_string(intrinsics[3])).c_str() : "None");
            ROS_INFO("cam%lu/resolution: %s", i, resolution.size() > 1 ? (std::to_string(resolution[0]) + " " + std::to_string(resolution[1])).c_str() : "None");
            ROS_INFO("cam%lu/out_resolution: %s", i, outResolution.size() > 1 ? (std::to_string(outResolution[0]) + " " + std::to_string(outResolution[1])).c_str() : "None");
            ROS_INFO("cam%lu/T_lidar_cam: ", i);

            if (distortionCoeffs.size() != 4 || intrinsics.size() != 4 || resolution.size() != 2 || outResolution.size() != 2) {
                ROS_ERROR("The number of distortionCoeffs, intrinsics, resolution and outResolution must be 4, 4, 2 and 2 respectively!");
            }
            double k1 = ((double)outResolution[0])/((double)resolution[0]);
            double k2 = ((double)outResolution[1])/((double)resolution[1]);
            CameraParam camParam(std::to_string(i + 1),
                                 cv::Size(outResolution[0], outResolution[1]),
                                 (cv::Mat_<double>(3, 3) << intrinsics[0]*k1, 0.0, intrinsics[2]*k1, 0.0, intrinsics[1]*k2, intrinsics[3]*k2, 0.0, 0.0, 1.0),
                                 (cv::Mat_<double>(1, 4) << distortionCoeffs[0], distortionCoeffs[1], distortionCoeffs[2], distortionCoeffs[3]));
            imageProcessors.emplace_back(camParam, outputDir);
        }
    }

    ~PreprocessorMetacam(){
        bag->close();
    }

    void IMUCallBack(const sensor_msgs::Imu::ConstPtr &msgIn) {
        sensor_msgs::Imu::Ptr msg(new sensor_msgs::Imu(*msgIn));
        msg->linear_acceleration.x*=g;
        msg->linear_acceleration.y*=g;
        msg->linear_acceleration.z*=g;

        bag->write(topicImuOut, msgIn->header.stamp, msg);
        waitPeriod = 0;
    }

    void LidarCallback(const sensor_msgs::PointCloud2::ConstPtr& msg) {
        ROS_INFO("In lidar callback");
        // 将 PointCloud2 转换为自定义的 PCL 点云类型
        pcl::PointCloud<edu_ros::Point> cloud;
        pcl::fromROSMsg(*msg, cloud);
    
        livox_ros_driver::CustomMsg customMsg;
        customMsg.header = msg->header;
        customMsg.point_num = cloud.points.size();
        customMsg.timebase = msg->header.stamp.toNSec();
        for (const auto& pt : cloud.points) {
            livox_ros_driver::CustomPoint custom_point;
            custom_point.x = pt.x;
            custom_point.y = pt.y;
            custom_point.z = pt.z;
            custom_point.reflectivity = static_cast<uint8_t>(pt.intensity);  // 将强度映射为反射率
            custom_point.offset_time = static_cast<uint32_t>((pt.time - cloud.points[0].time) * 1e6); // 时间偏移，单位转为微秒
            custom_point.tag = pt.tag;  // 标签
            custom_point.line = pt.line;  // 激光编号
    
            customMsg.points.push_back(custom_point);
        }
        bag->write(topicLidarOut, msg->header.stamp, customMsg);
        waitPeriod = 0;
    }

    void Run()
    {
        ros::Rate rate(100);
        while (ros::ok())
        {
            rate.sleep();
            ros::spinOnce();
            if (waitPeriod >= 0 && ++waitPeriod > 1000) {
                ROS_WARN("No messages received for 1000 cycles, shutting down.");
                return;
            }
        }
    }

    void ReadMessage()
    {
        auto bagPaths = GetFilesInDir(bagDir, 'bag');
        // 打开输入bag文件
        rosbag::Bag inputBag;

        for (const auto& bagPath : bagPaths) {
            rosbag::Bag inputBag;
            try {
                inputBag.open(bagPath, rosbag::bagmode::Read);
            } catch (rosbag::BagException& e) {
                std::cerr << "Error opening input bag: " << e.what() << std::endl;
                return 1;
            }
            // 处理所有消息
            rosbag::View viewAll(inputBag, rosbag::TopicQuery());
            for (const rosbag::MessageInstance& m : viewAll) {
                // 将原消息写入输出
                out_bag.write(m.getTopic(), m.getTime(), m);

                // 处理图像消息
                if (m.getTopic() == image_topic) {
                    sensor_msgs::Image::ConstPtr img_msg = m.instantiate<sensor_msgs::Image>();
                    if (img_msg) {
                        try {
                            // 转换为OpenCV图像
                            cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(img_msg, sensor_msgs::image_encodings::BGR8);
                            cv::Mat rectified;
                            camera_model.rectifyImage(cv_ptr->image, rectified);

                            // 转换回ROS消息
                            cv_bridge::CvImage rectified_cv(img_msg->header, "bgr8", rectified);
                            sensor_msgs::Image rectified_msg;
                            rectified_cv.toImageMsg(rectified_msg);

                            // 写入处理后的图像到新话题
                            out_bag.write(output_image_topic, m.getTime(), rectified_msg);
                        } catch (const cv_bridge::Exception& e) {
                            std::cerr << "Image processing error: " << e.what() << std::endl;
                        }
                    }
                }
            }
        }
    }
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "adapter_metacam");
    std::shared_ptr<ros::NodeHandle> nh = std::make_shared<ros::NodeHandle>("~");

    PreprocessorMetacam preprocessorMetacam(nh, true);

    preprocessorMetacam.Run();
    return 0;
}