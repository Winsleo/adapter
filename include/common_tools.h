#ifndef __COMMON_TOOLS_H__
#define __COMMON_TOOLS_H__

#include <filesystem>
#include "tools_color_printf.hpp"

namespace CommonTools
{
    inline bool IfFileExist(const std::string &name)
    {
        return std::filesystem::exists(name);
    };
    inline void IfNotExistThenCreate(const std::string &name){
        if(!CommonTools::IfFileExist(name))
        {
            std::cout << ANSI_COLOR_BLUE_BOLD << "Create dir: " << name << ANSI_COLOR_RESET << std::endl;
            std::filesystem::create_directories(name);
        }
    }

    inline std::vector<std::string> GetFilesInDir(const std::string &dir, const std::string &extension)
    {
        if (!std::filesystem::exists(dir))
        {
            return {};
        }
        std::vector<std::string> files;
        std::filesystem::path dirPath(dir);
        std::filesystem::directory_iterator dirItr(dirPath);
        for (const auto &entry : dirItr)
        {
            auto fsPath = entry.path();
            if (fsPath.extension() == extension)
            {
                files.emplace_back(fsPath.string());
            }
        }
        std::sort(files.begin(), files.end());
        return files;
    }

    inline std::vector<std::string> SplitString(const std::string& str, const char& sep) {
        // Split the string by 
        std::vector<std::string> strSplit;
        std::stringstream ss(str);
        std::string item;
        while (std::getline(ss, item, sep)) {
            strSplit.push_back(item);
        }
        return strSplit;
    }
}
#endif
